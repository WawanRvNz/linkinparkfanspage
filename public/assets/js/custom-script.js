myAnimate = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       60,          // default
      mobile:       true,       // default
      live:         true        // default
    }
)
myAnimate.init();

$(document).ready(function(){
	$('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});