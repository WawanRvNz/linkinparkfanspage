<?php

class postingcontroller extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$postingcontroller=post::all();
		return View::make('viewPost', compact('postingcontroller'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array (
		'title'  =>  'required',
		'content'  =>  'required',
		'image'  =>  'required'
		);
		$validation = Validator::make(Input::all(), $rules);
		if ($validation->fails())
		{
			return Redirect::to('addPost')->withErrors($validation)->withInput();
		}
		else
		{
			$data=Input::all();
			if (Input::hasFile('image')) {
				Input::file('image')->move('files', Input::file('image')->getClientOriginalName());
				$data['image'] = Input::file('image')->getClientOriginalName();
			}
			post::create($data);
			return Redirect::to('welcome');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
			post::find($id)->delete();
			return Redirect::to('viewPost');
	}


}
