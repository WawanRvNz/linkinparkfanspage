<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Linkin Park Official Fans Page</title>

<!-- Bootstrap Assets -->
{{ HTML::style('assets/css/bootstrap.css') }}

<!-- Themes Assets -->
{{ HTML::style('assets/css/tema_style.css') }}

<!-- Animation Assets -->
{{ HTML::style('assets/css/animasitext.css') }}

<!-- Image Assets -->
{{ HTML::style('assets/css/style-gambar.css') }}

<!-- Lightbox Assets -->
{{ HTML::style('assets/css/stylelighbox.css') }}

<!-- Carousel Assets -->
{{ HTML::style('assets/carousel/carousel.css') }}
{{ HTML::style('assets/carousel/theme.css') }}

{{ HTML::script('assets/js/jquery-1.11.1.js') }}
</head>
<body>
<!-----Navigation--------->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
        <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav pull-left">
                    <img src="{{ asset('assets/img/linkinlogo.png') }}">
                </ul>
                <ul class="nav navbar-nav pull-right">
                    <li>
						<a class="page-scroll" href="#page1">Home</a>
					</li>
                    <li>
                        <a class="page-scroll" href="#page2">Profile</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#page3">News</a>
                    </li>
					<li>
                        <a class="page-scroll" href="#page5">Gallery</a>
                    </li>
					<li>
                        <a class="page-scroll" href="#page6">Join US</a>
                    </li>
					<li>
                        <a class="page-scroll" href="#page8">Contact</a>
                    </li>
                </ul>
         </div>
     </div>
</nav>
<!---------------------------------------------->
<div class="tema-page1" id="page1">
    <div class="container">
        <div id="isi-page1" class="titlepage">
        	<h1 class="wow fadeInDown" data-wow-duration="2s">WELCOME TO</h1>
            <p class="wow fadeInUp"  data-wow-duration="2s">LINKIN PARK OFFICIAL FANS PAGE</p>
            <div class="wow flipInX" data-wow-delay="2s"><a href="#page2" class="btn-next-page btn-home-start page-scroll"><span class="glyphicon glyphicon-circle-arrow-down"></span></a></div>
            </div>
        </div>

    </div>
</div>

<div class="tema-page2" id="page2">
    <div class="container">
		<br><br>
    	<div class="txt-page2">
        	<h1 class="wow fadeInDown" data-wow-duration="2s">Linkin Park Crew</h1>
			<div class="center-line wow bounceInLeft" data-wow-duration="2s"></div>
        	<h4 class="wow fadeInDown" data-wow-duration="2s">
			WE ARE NOT SATISFIED. WE ARE HUNGRY.<br>
			HUNGRY FOR THE VISCERAL. CATHARTIC. INSPIRED.<br>
			DEFIANT. WE ARE NOT HEROES OR ANTI-HEROES.<br>
			WE CARRY ONLY THE FLAG THAT IS OUR OWN.<br>
			NOW IS NOT THE TIME TO LOOK BACK TO SEE IF<br>
			ANYONE IS FOLLOWING.<br>
			NOW IS THE TIME TO CHARGE FORWARD INTO THE UNKNOWN.<br><br>

			THE HUNTING PARTY.<br>
			</h4>
        </div>
        	<p align="center"><img src="{{ asset('assets/img/crew.png') }}"/></p>
    </div>
</div>
<div class="tema-page3" id="page3">
    <div class="container">
	<br><br>
        <div class="txt-page3">
        	<h1 class="wow fadeInDown" data-wow-duration="2s">NEWS</h1><br>
   			<div class="center-line wow bounceInLeft" data-wow-duration="2s"></div>
        </div>
<?php foreach($postingcontroller as $row): ?>
        <div class="col-md-3" >
		<a href="{{ URL::to('news') }}">
			<img src="{{ URL::to('files/'.$row->image) }}" width="224" height="194" style="margin-top:50px;">
		</a>
			<h3><?=$row->title?></h3>
			<p> <?= substr($row->content, 50) ?> </p>
			<div class="col-xs-offset-7 col-md-2">
           	<a href="{{ URL::to('news') }}"><input type="submit" name="btn" class="btn btn-info btn-sm" value="Read-More"/></a>
            </div>
		</div>
<?php endforeach; ?>
</div>
</div>

<div class="tema-page5" id="page5">
	<div class="container">
	<br><br>
    	<div class="txt-page5">
         <h1 class="wow fadeInDown" data-wow-duration="2s">Gallery</h1><br />
			<div class="center-line wow bounceInLeft" data-wow-duration="2s"></div><br />
            <div id="owl-demo" class="owl-carousel">
				 <div id="gallery" class="img">
                   <div class="item wow fadeInUpBig" data-wow-duration="2s">
                      <a href="{{ asset('assets/img/img1.jpg')}}" class="lightbox" title="Linkin Park 1">
                      	<div class="hover"><p>Linkin Park 1</p></div>
                      	<img class="lazyOwl" src="{{ asset('assets/img/img1.jpg')}}" width="224" height="194">
                      </a>
                    </div>
                 </div>

				 <div id="gallery" class="img">
                   <div class="item wow fadeInUpBig" data-wow-duration="2s">
                      <a href="{{ asset('assets/img/img2.jpg')}}" class="lightbox" title="Linkin Park 2">
                      	<div class="hover"><p>Linkin Park 2</p></div>
                      	<img class="lazyOwl" src="{{ asset('assets/img/img2.jpg')}}" width="224" height="194">
                      </a>
                    </div>
                 </div>

				 <div id="gallery" class="img">
                   <div class="item wow fadeInUpBig" data-wow-duration="2s">
                      <a href="{{ asset('assets/img/img3.jpg')}}" class="lightbox" title="Linkin Park 3">
                      	<div class="hover"><p>Linkin Park 3</p></div>
                      	<img class="lazyOwl" src="{{ asset('assets/img/img3.jpg')}}" width="224" height="194">
                      </a>
                    </div>
                 </div>

				 <div id="gallery" class="img">
                   <div class="item wow fadeInUpBig" data-wow-duration="2s">
                      <a href="{{ asset('assets/img/img4.png')}}" class="lightbox" title="Linkin Park 4">
                      	<div class="hover"><p>Linkin Park 4</p></div>
                      	<img class="lazyOwl" src="{{ asset('assets/img/img4.png')}}" width="224" height="194">
                      </a>
                    </div>
                 </div>

				 <div id="gallery" class="img">
                   <div class="item wow fadeInUpBig" data-wow-duration="2s">
                      <a href="{{ asset('assets/img/img5.jpg')}}" class="lightbox" title="Linkin Park 5">
                      	<div class="hover"><p>Linkin Park 5</p></div>
                      	<img class="lazyOwl" src="{{ asset('assets/img/img5.jpg')}}" width="224" height="194">
                      </a>
                    </div>
                 </div>
            </div>
		</div>
    </div>
</div>
<div class="tema-page6" id="page6">
	<div class="container">
	<br><br>
    	<div class="txt-page6">
         <h1 class="wow fadeInDown" data-wow-duration="2s">Join Us </h1><br />
			<div class="center-line wow bounceInLeft" data-wow-duration="2s"></div><br />
            <div class="box-dampak">
				<div class="home-btn" style="margin-top:320px; margin-left:80px;">
				  <a class="btn btn-primary btn-lg" href='{{ URL::to('register') }}'> Join US In Linkin Park Underground</a></li>      
				</div>
            </div>
        </div>
    </div>
</div>
<div class="tema-page7" id="page8">
	<div class="container">
    	<div class="txt-footer" id="footer">
        	<h1>Contact</h1>
            <p>Visit www.OnGuardOnline.gov <br> for social networking safety tips for parents and youth.</p>
            <section>
              <ul id='services'>
                <h2>Follow Us On</h2>
                <li>
                  <div class="facebook wow zoomIn"></div>
                  <span class=" wow flipInX" data-wow-delay="0.5s">Facebook</span>
                </li>
                <li>
                  <div class="twitter wow zoomIn" data-wow-delay="1s" ></div>
                  <span class=" wow flipInX" data-wow-delay="1.5s">Twitter</span>
                </li>
                <li>
                  <div class="googleplus wow zoomIn" data-wow-delay="2s"></div>
                  <span class=" wow flipInX" data-wow-delay="2.5s">Google+</span>
                </li>
                <li>
                  <div class="youtube wow zoomIn" data-wow-delay="3s"></div>
                  <span class=" wow flipInX" data-wow-delay="3.5s">Youtube</span>
                </li>
              </ul>
            </section>
            <h5>&copy Copyright 2015. Linkin Park Official Fans Page - All Right Reserved. </h5>
        </div>
    </div>
</div>
<!--------------------- Java Script Caraousel------------------->
{{ HTML::script('assets/js/jquery-1.9.1.min.js') }}
{{ HTML::script('assets/carousel/carousel.js') }}
<script>
    $(document).ready(function() {

      $("#owl-demo").owlCarousel({
        items : 4,
        lazyLoad : true,
        navigation : true,
		autoPlay : true,
		stopOnHover : true,
      });

    });
</script>
<!--------------------- Java Script ------------------->
{{ HTML::script('assets/js/bootstrap.min.js') }}
{{ HTML::script('assets/js/wow.min.js') }}
{{ HTML::script('assets/js/jquery.easing.min.js') }}
{{ HTML::script('assets/js/custom-script.js') }}

<!--------------------- Java Script Lightbox------------------->
{{ HTML::script('assets/js/jquery-1.4.js') }}
{{ HTML::script('assets/lightbox/jquery.lightbox.min.js') }}
<script type="text/javascript">
	var f=jQuery.noConflict();
		f(document).ready(function()
		{
			 f('.lightbox').lightbox();
		});
</script>
{{ HTML::script('assets/js/jquery.tools.min.js') }}
<script type="text/javascript">
    var t=jQuery.noConflict();
      t(document).ready(function()
	  {
      	t("ul.tabs").tabs("div.panes > div");
	   });
</script>
</body>
</html>
