@extends('media')
@section('content')
@section('title', $judul)

<!---------------------------------------------->
<div class="tema-pageadmin1">
    <div class="container">
        <div id="isi-page1" class="titlepagereg">
		<p> LOGIN MEMBER </p>
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<form action="{{ URL::to('auth/loginuser') }}" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<div class="row">
									<div class="col-xs-12">
										<label for="user">Username*</label>
									</div>
									<div class="col-xs-6">
										<input type="text" name="username" placeholder="Username" required="required" class="form-control"/>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-xs-12">
										<label for="pass">Password*</label>
									</div>
									<div class="col-xs-6">
										<input type="text" name="password" placeholder="Password" required="required" class="form-control"/>
									</div>
								</div>
							</div>
						
							<div class="home-btn" style="margin-top:40px;">
                                <button type="submit" name="submit" class="btn btn-primary btn-md">Login</button>
                                <button type="reset" name="cancel" class="btn btn-primary btn-md">Cancel</button>  
							</div>
						</form>
					</div>
				</div>
			</div>

        </div>
    </div>  
</div>
@stop