@extends('media')
@section('content')
@section('title', $judul)

<!---------------------------------------------->
<div class="tema-pageadmin1">
    <div class="container">
        <div id="isi-page1" class="titlepagereg">
		<p> REGISTRASI MEMBER </p>
		<h3> *Required Fields </h3>
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<form method="post" enctype="multipart/form-data">
							<div class="form-group">
								<div class="row">
									<div class="col-lg-12">
										<label for="nama">Full Name*</label>
									</div>
									<div class="col-xs-6">
										<input type="text" name="fullname" placeholder="Full Name" class="form-control" required="required" />
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-xs-12">
										<label for="almt">Email*</label>
									</div>
									<div class="col-xs-6">
										<input type="email" name="email" placeholder="Email" required="required"  class="form-control"/>
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-xs-12">
										<label for="almt">Country</label>
									</div>
									<div class="col-xs-6">
										<input type="text" name="country" placeholder="Country"  class="form-control"/>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-xs-12">
										<label for="user">Username*</label>
									</div>
									<div class="col-xs-6">
										<input type="text" name="username" placeholder="Username" required="required" class="form-control"/>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-xs-12">
										<label for="pass">Password*</label>
									</div>
									<div class="col-xs-6">
										<input type="text" name="password" placeholder="Password" required="required" class="form-control"/>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-xs-12">
										<label for="foto">Photo</label>
									</div>
									<div class="col-xs-6">
										<input type="file" name="image"  class="form-control"/>
									</div>
								</div>
							</div>
							
							<div class="home-btn" style="margin-top:40px;">
							 <input type="submit" name="submit" value="Registration" class="btn btn-primary btn-md"  />     
							 <input type="reset" name="cancel" value="Cancel" class="btn btn-primary btn-md"  />    
							</div>
						</form>
					</div>
				</div>
			</div>

        </div>
    </div>  
</div>
@stop