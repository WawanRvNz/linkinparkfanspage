<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>.: System Administrator :.</title>
		{{ HTML::style('http://fonts.googleapis.com/css?family=Roboto:400,100,300,500') }}
		{{ HTML::style('admin/assets/bootstrap/css/bootstrap.min.css') }}
		{{ HTML::style('admin/assets/font-awesome/css/font-awesome.min.css') }}
		{{ HTML::style('admin/assets/css/form-elements.css') }}

		{{ HTML::style('admin/assets/css/style.css') }}
		{{ HTML::style('admin/assets/css/form-elements.css') }}
		{{ HTML::style('admin/assets/css/form-elements.css') }}
 </head>
<body>
        <div class="top-content">
                <div class="container">
                        <br><br>
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>WELCOME TO LP Fans Page ADMINISTRATOR</h3>
                            		<p>Enter your username and password to log on</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-key"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
								<form action="{{ URL::to('auth/login') }}" method="post" enctype="multipart/form-data">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Username</label>
			                        	<input type="text" name="username" placeholder="Username..." required class="form-username form-control" id="form-username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Password</label>
			                        	<input type="password" name="password" placeholder="Password..." required class="form-password form-control" id="form-password">
			                        </div>
			                        <button type="submit" name="submit" class="btn">Sign in</button>
			                    </form>
		                    </div>
                       </div>
                       <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <div class="description">
                            	<p>
	                            	 Copyright © 2015. Linkin Park Administrator | All Right Reserved
                            	</p>
                            </div>
                        </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>
</body>
</html>
