<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: LP Administrator ::</title>
{{ HTML::style('assets/css/bootstrap.css') }}
{{ HTML::style('assets/css/themes.css') }}
{{ HTML::style('assets/fa/font-awesome.min.css') }}
{{ HTML::script('assets/js/jquery-1.11.1.js') }}
<script>
var $ = jQuery.noConflict();
$(function() {
  var Accordion= function(el, multiple) {
    this.el = el || {};
    this.multiple = multiple || false;

    var links = this.el.find('.link');
    links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
  }

  Accordion.prototype.dropdown = function(e) {
    var $el = e.data.el;
      $this = $(this),
      $next = $this.next();

    $next.slideToggle();
    $this.parent().toggleClass('open');

    if (!e.data.multiple) {
      $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
    };
  }

  var accordion = new Accordion($('#accordion'), false);
});
</script>
</head>
<body>
<div class="sidebar">
		<div class="logo clear"><p align="center"></p><br />
        </div>
		<div class="menu">
			<ul><li><a href="#">MENU UTAMA</a>
					<ul id="accordion" class="accordion">
					  <li>
						<div class="link"><i class="fa fa-tachometer"></i>Dashboard<i class="fa fa-chevron-down"></i></div>
						<ul class="submenu">
						  <li><a href="{{ URL::to('welcome') }}">Welcome</a></li>
						</ul>
					  </li>
					  <li>
						<div class="link"><i class="fa fa-rss"></i>Posting<i class="fa fa-chevron-down"></i></div>
						<ul class="submenu">
						  <li><a href="{{ URL::to('viewPost') }}">All Post in Publish &nbsp;</a></li>
						  <li><a href="{{ URL::to('addPost') }}">Add New</a></li>
						</ul>
					  </li>
					  <li>
						<div class="link"><i class="fa fa-rss"></i>Comments<i class="fa fa-chevron-down"></i></div>
						<ul class="submenu">
						  <li><a href="{{ URL::to('viewComments') }}">All Comments&nbsp;</a></li>
						</ul>
					  </li>
					  <li>
						<div class="link"><i class="fa fa-user-md"></i>Profil<i class="fa fa-chevron-down"></i></div>
						<ul class="submenu">
						  <li><a href="{{ URL::to('profil') }}">Your Profil</a></li>
						</ul>
					  </li>
					</ul>
			</li>
		  </ul>
	  </div>
	</div>

<div class="main">
   <div class="main-wrap">
	<div class="header clear">
	    <ul class="links clear">
		<li>::::  &nbsp<strong>Selamat Datang, {{ Auth::user()->fullname }}</strong> ::::&nbsp;</li>
		<li><img src="{{ asset('assets/images/home.png') }}" alt="" class="icon" /><span class="text"><a href="{{ URL::to('index') }}" target="_blank"><b>Preview</b></span></a></li>
		<li><img src="{{ asset('assets/images/out.png') }}" alt="" class="icon" /><span class="text"><a href="{{ URL::to('auth/logout') }}"><b>Logout</b></span></a></li>
		</ul>
	 </div>
  </div>
</div>
@yield('content')
</body>
</html>
