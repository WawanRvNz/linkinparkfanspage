@extends('media')
@section('content')
@section('title', $judul)

<!---------------------------------------------->
<div class="tema-pageadmin1">
    <div class="container">
		<div class="modal-header">
           <p style="margin-bottom:-8px;"> NEWS DETAILS </p>
        </div>
		<div class="col-lg-12" style="margin-top:20px; margin-left:-20px;">
                <div class="col-xs-7">
                	<img width="100%" src="{{asset('assets/img/news1.jpg')}}" />
                </div>
				<div class="col-lg-7">
                	<h4 style="margin-top:20px; font-weight:bold;">LPTV: EUROPE '15 - PART 1</h4>
					In this episode of LPTV, Linkin Park kicks of the European tour in Belgium, In this episode of LPTV, Linkin Park kicks of the European tour in Belgium, In this episode of LPTV, Linkin Park kicks of the European tour in Belgium, In this episode of LPTV, Linkin Park kicks of the European tour in Belgium, In this episode of LPTV, Linkin Park kicks of the European tour in Belgium, In this episode of LPTV, Linkin Park kicks of the European tour in Belgium, In this episode of LPTV, Linkin Park kicks of the European tour in Belgium, In this episode of LPTV, Linkin Park kicks of the European tour in Belgium, In this episode of LPTV, Linkin Park kicks of the European tour in Belgium, 
                </div>
            </div>
		</div>
		<div class="col-lg-12" style="margin-top:20px;">
            	<div class="container">
                 <div class="col-lg-7">
                    	<h4 style="margin-left:-20px;">Leave Comments</h4>
                    </div>
            		@if(!Auth::check())
                    <div class="col-lg-7">
                    	<h5 style="margin-left:-20px;"><b>You Must <a href="{{ URL::to('signin') }}"> Login </a> First</b></h5>
                    </div>
                    @else
                    	<div class="col-lg-7">
                    		<h4 style="margin-left:-20px;">Hello , {{ Auth::user()->fullname }}.</h4>
						</div>	
                        <form method="post" enctype="multipart/form-data">
                        <div class="col-lg-7" style="margin-left:-20px;">
                            <textarea id="comments" name="comments" cols="100" rows="5"></textarea>
                        </div>
                        <div class="col-lg-7" style="margin-top:20px; margin-left:-20px; margin-bottom:20px;">
                            <input type="submit" name="submit" class="btn btn-info btn-sm" value="Submit"/>
                        </div>
                     	</form>
                     @endif
				</div>
                <div class="modal-header">
                   <p style="margin-bottom:-8px; margin-left:-8px;"> Comments Display </p>
                </div><br />
                <table width="700" border="0">
                 <?php foreach($commentscontroller as $row): ?>
                        <tr height="110">
                          <td width="113" align="center"><img src="{{URL::to('files/'.$row->image)}}" width="80" height="80"/>
                          </td>
                       	  <td width="571">
						  	<font size="+1"><b><?=$row->fullname?></b></font><br />
                         	<font size="-2"><?=$row->tanggal?></font><br />
                        	<?=$row->comments?><br /><br /><br />
                          </td>
                         </tr>
                        <?php endforeach; ?>
                </table>
                {{ $commentscontroller->links() }}
		</div>
        </div>
    </div>  
</div>
@stop