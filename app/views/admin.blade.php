<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: LP Administrator ::</title>
{{ HTML::style('assets/css/themes.css') }}
{{ HTML::style('assets/fa/font-awesome.min.css') }}
{{ HTML::style('assets/js/jquery-1.11.1.js') }}
<script>
$(function() {
  var Accordion= function(el, multiple) {
    this.el = el || {};
    this.multiple = multiple || false;

    var links = this.el.find('.link');
    links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
  }

  Accordion.prototype.dropdown = function(e) {
    var $el = e.data.el;
      $this = $(this),
      $next = $this.next();

    $next.slideToggle();
    $this.parent().toggleClass('open');

    if (!e.data.multiple) {
      $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
    };
  }  

  var accordion = new Accordion($('#accordion'), false);
});
</script>
</head>
<body>
<div class="sidebar">
		<div class="logo clear"><p align="center"></p><br />
        </div>	
		<div class="menu">
						  <ul><li><a href="#">MENU UTAMA</a>
							  <ul id="accordion" class="accordion">
				  <li>
					<div class="link"><i class="fa fa-tachometer"></i>Dashboard<i class="fa fa-chevron-down"></i></div>
					<ul class="submenu">
					  <li><a href="media.php">Welcome</a></li>
					</ul>
				  </li>
				  <li>
					<div class="link"><i class="fa fa-rss"></i>Posting<i class="fa fa-chevron-down"></i></div>
					<ul class="submenu">
					  <li><a href="media.php?page=Posting">All Post in Publish &nbsp;</a></li>
					  <li><a href="media.php?page=Posting&action=bin">All Post in Bin &nbsp;</a></li>
					  <li><a href="media.php?page=Posting&action=add">Add New</a></li>
					</ul>
				  </li>
				  <li>
					<div class="link"><i class="fa fa-picture-o"></i>Media<i class="fa fa-chevron-down"></i></div>
					<ul class="submenu">
					  <li><a href="media.php?page=Media">All Media</a></li>
					  <li><a href="media.php?page=Media&action=add">Add New</a></li>
					</ul>
				  </li>
					<li>
					<div class="link"><i class="fa fa-user-md"></i>Profil<i class="fa fa-chevron-down"></i></div>
					<ul class="submenu">
					  <li><a href="media.php?page=Profil">Your Profil</a></li>
					  <li><a href="media.php?page=Profil&action=edit">Edit Your Profil</a></li>
					</ul>
				  </li>
				   <li>
					<div class="link"><i class="fa fa-unlock-alt"></i>Password<i class="fa fa-chevron-down"></i></div>
					<ul class="submenu">
					  <li><a href="password.php">Change Password</a></li>
					</ul>
				  </li>
				</ul>
			</li>
		  </ul>
	  </div> 
	</div>

<div class="main">
   <div class="main-wrap">
	<div class="header clear">
	    <ul class="links clear">
		<li>::::  &nbsp<strong>Selamat Datang,</strong> ::::&nbsp;</li>
		<li><a href="process/out.php">
        	<img src="images/out.png" alt="" class="icon" /><span class="text"><b>Logout</b></span></a></li>
		</ul>
	 </div>
  </div>
</div>
<p align="center"><font face="Times New Roman" size="+4" color="#000000"> <b> WELCOME TO F5 ADMINISTRATOR </font></b></p>
	<table align="center" border="2" bgcolor="#14b9d5" width="990" bordercolor="#FF0000" id="content-data">
    <tr>
    <td id="content-data">
    <table border="0" align="center" cellpadding="0" cellspacing="0" height="250">
              <tr>
                <td width="980" height="30" align="left" valign="top"><p class="copy">&nbsp;</p>
                  <p class="copy"><font face="Times New Roman" color="#000000"> <b> Selamat Datang </b></font></p><br />
                  <p class="copy">&nbsp;&nbsp;&nbsp;&nbsp;
                  	Hai <b><i>  di halaman Administrator.<br />		  
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    Silahkan klik menu pilihan yang berada di sebelah kiri untuk mengelola konten website anda. 
                  <p class="copy">&nbsp;</p>
                  <p class="copy">&nbsp;</p>
                  <p class="copy">&nbsp;</p>
                  <p class="copy">&nbsp;</p>
                  <p class="copy">&nbsp;</p>
                  <p class="copy">&nbsp;</p>
                  <p class="copy">&nbsp;</p>
                  <p class="copy">&nbsp;</p>
                  <p class="copy">&nbsp;</p>
                  <p class="copy">&nbsp;</p>
                  <p class="copy">&nbsp;</p>
                <p class="copy" align="right">
				</p></td>
              </tr>
      </table>        
    </td>
    </tr>
    </table>
	
</div>
</body>
</html>
