<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::resource('users', 'UserController'); //untuk membuat metode secara langsung di route

/*LOGIN ADMIN*/

Route::post('auth/login', array('uses' => 'users@doLogin'));

Route::get('auth/logout', array('uses' => 'users@doLogout'));

/*LOGIN USER*/

Route::post('auth/loginuser', array('uses' => 'users@doLoginuser'));

Route::get('auth/logoutuser', array('uses' => 'users@doLogoutuser'));

/*HALAMAN INDEX*/

Route::get('/', function()
{
	return Redirect::to('index');
});
Route::get('index', function()
{
	$postingcontroller=post::all();
	return View::make('index', compact('postingcontroller'));
});

/*HALAMAN ADMINISTRATOR*/

Route::get('administrator', function()
{
	return View::make('administrator');
});

/*HALAMAN NEWS*/

Route::post('news', 'commentscontroller@store');
Route::get('news', function()
{
	$judul = 'Linkin Park Fans Page';
	$commentscontroller=Comments::all();
	$commentscontroller=Comments::paginate(3);
	return View::make('news', compact('judul'), compact('commentscontroller'));
});

/*HALAMAN SIGN IN*/

Route::get('signin', function()
{
	$judul = 'Linkin Park Fans Page';
	return View::make('signin', compact('judul'));
});

/*HALAMAN REGISTER*/

Route::post('register', 'users@store');
Route::get('register', function()
{
	$judul = 'Linkin Park Fans Page';
	return View::make('register', compact('judul'));
});

/*HALAMAN HOME ADMIN*/

Route::get('home', array('before' => 'logged', 'uses' => function()
{
	return View::make('welcome');
}));

/*MENU ADMIN*/

Route::get('welcome', function()
{
	return View::make('welcome');
});

Route::post('addPost', 'postingcontroller@store');
Route::get('addPost', function()
{
	return View::make('addPost');
});
Route::get('delete/{id}','postingcontroller@destroy');
Route::get('viewPost', function()
{
	$postingcontroller=post::all();
	$postingcontroller=post::paginate(3);
	return View::make('viewPost', compact('postingcontroller'));
	
});

Route::get('profil', function()
{
	return View::make('profil');
});


Route::get('delete1/{id}','commentscontroller@destroy');
Route::get('viewComments', function()
{
	$commentscontroller=Comments::all();
	$commentscontroller=Comments::paginate(4);
	return View::make('viewComments', compact('commentscontroller'));
	
});