-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 11 Okt 2015 pada 11.23
-- Versi Server: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_pwl_uts`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
`id` int(5) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `image` varchar(100) NOT NULL,
  `comments` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `comments`
--

INSERT INTO `comments` (`id`, `fullname`, `image`, `comments`, `tanggal`) VALUES
(1, 'Dwi Yanti', 'DSC_0411_resize.JPG', 'i love Linkin Park, dunno why they stopped, used to listen to them while going to college every single day , fuck them they left me alone ', '2015-10-11 05:15:17'),
(2, 'Wawan Rahmawan', 'IMG_20150727_212940.JPG', 'This is for the people who are hurt or a social outcast. There are people that care. Forget the hurt and move forward. If you dont move on for yourself, move on for the happiness of others. Have a good week and dont give up', '2015-10-11 05:17:23'),
(3, 'Andi Radita', 'HTML5_Wallpaper_2560x1440.png', 'Take me down to the river bend, Take me down to the fighting end, Wash the poison from off my skin. Show me how to be whole again..... #linkinpark  ? #goodmorning  ', '2015-10-11 05:18:41'),
(4, 'Wawan Rahmawan', 'IMG_20150727_212940.JPG', 'That''s why this is the "prequel to Hybrid Theory", because it prefaces all of their work and describes things going on behind the scenes long before they became famous.\r\nDid noone notice how incredibly deep A Thousand Suns was? THP is just as much so.', '2015-10-11 05:21:22'),
(5, 'okariyadi', 'IMG_20151003_135854.jpg', 'As a detail-oriented person that straddles the line dividing the Pitchfork and radio rock universes, it always baffles me when writers fail to grasp basic elements of a band''s sound (usually something dismissively popular like Linkin Park). The things in "Keys to the Kingdom" that are ''new'' are things the band has been doing for a while, and the things described as ''typical'' are conversely not really like anything they have done in the past. anyway. ', '2015-10-11 05:22:01'),
(7, 'Yuni Antari', 'SAM_0567_resize.jpg', 'I agree i think this Album is actually taking LP a step in the past leaning towards Hybrid Theory. I think when they did A Thousand Suns they became known to a lot more people ', '2015-10-11 05:29:15'),
(8, 'Dwi Yanti', 'DSC_0411_resize.JPG', 'The Hunting Party is the sixth studio album by the American rock band Linkin Park. The album, produced by band members Mike Shinoda and Brad Delson, was released by Warner Bros. Records and Machine Shop on June 13, 2014. ', '2015-10-11 05:30:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
`id` int(5) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `image`) VALUES
(1, 'LPTV: EUROPE ''15 - PART 1', 'In this episode of LPTV, Linkin Park kicks of the European tour in Belgium, Mike shares plans for th', 'news1.jpg'),
(2, 'FORT MINOR LIVE REVIEW', 'Returning to the UK after a ten year hiatus, Fort Minor’s sole member and Linkin Park creative force', 'news2.png'),
(3, 'LPTV: CHINA ''15 - PART 2', 'In this episode of LPTV, Chester gives a heartfelt thank you to the crew for their hard work, Mike v', 'news3.jpg'),
(5, 'LPTV: CHINA ''15 - PART 1', 'In this episode, the band arrives in China to a warm welcome at the Mercedes Benz press conference a', 'news4.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(5) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(60) NOT NULL,
  `image` varchar(100) NOT NULL,
  `role` enum('0','1') NOT NULL,
  `remember_token` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `fullname`, `email`, `country`, `username`, `password`, `image`, `role`, `remember_token`) VALUES
(1, 'Wawan', 'wawan@wawan.com', 'Indonesia', 'wawan', '$2y$10$SfFD0Og7fCgJ2S7J2i5RL.vjdAfwviy8OTz3R7/9n.3SH7BQtGncq', '', '1', 'I7gUFrss4fRleiwRcaXb5OFBiVMYe2AoyjErBBrCLMSj7YP9cNBE7bMEou9o'),
(9, 'Andi Radita', 'andirad@gmail.com', 'Yogyakarta', 'andir', '$2y$10$idGrxywUSomXrUk0d.4oSu1O1rCp2rrvo2B5c7xKZV4mf8p6dloP2', 'HTML5_Wallpaper_2560x1440.png', '0', 'JBronSuDLmOisgDcQym1VrisLAOdWDY2artVHC1qfx9xEmDXCq3w0yR7jg1C'),
(10, 'okariyadi', 'okads@ymail.com', 'Jakarta', 'oka', '$2y$10$M9.qwq2r2GOua8AJ1uzFvOZNfrERKMU76uJmUOAHuXKFph227FmOu', 'IMG_20151003_135854.jpg', '0', 'G1ryg7B90FIL4PH7kEgJDYafXUrT27PAVd5NRvTmPB7Bfc3pvH1fNU3MbH8m'),
(11, 'Dwi Yanti', 'Dwiy@rocketmail.com', 'Medan', 'dwi', '$2y$10$CZnFSJJ/.e4Jv6o4TOyrWeM.tBLuqHtFxQ8SrzqDJKKeViOKtiqmS', 'DSC_0411_resize.JPG', '0', 'YToyHercgy64mdYo3cWvlUmkGSKyoEvXd7soMYmUCGJM7W7BClHJuGRHYS5D'),
(12, 'Wawan Rahmawan', 'wawan_linkerz@yahoo.co.id', 'Bali', 'wawandxd', '$2y$10$rlf3cggYGhI6LOHWy.tuQ.ENHiw5EhiNc4QS/CIZO66bLELw2aAWK', 'IMG_20150727_212940.JPG', '0', 'aKtDPR9cXZAogoEr236cEdK2PWmZ1hHySjhLONL1XiVgANpoBSrN4bSAJ8g6'),
(13, 'Yuni Antari', 'yuni@yahoo.com', 'Bali', 'yuni', '$2y$10$MP.KhLHS.TVAFLYvTSaQVuSEBxhMqSikVqbnhVpmKgnLGKdlA96XW', 'SAM_0567_resize.jpg', '0', 'cLjxUBst8lYbhImSDLIfd7A7dmucF3NDGBtdLbTh5NxIaI6nETvATub60pFh');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
